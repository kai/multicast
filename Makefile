all: listener sender

listener: listener.o
	gcc -o listener listener.o

sender: sender.o
	gcc -o sender sender.o

.c.o:
	gcc -c -Wall -o $@ $<

clean:
	rm -f listener sender *.o
