/* Multicast test, listener */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))

#define V6_MULTICAST_ADDR "ff02::1:3"
#define V4_MULTICAST_ADDR "224.0.0.252"
#define IFACE_NAME "br0"
#define LLMNR_PORT 5355

static int setup_v6_socket(const char *multicast_addr,
                           const char *iface_name,
                           int port)
{
    int err;
    int sock6 = -1;
    int yes = 1;
    struct sockaddr_in6 name6;
    ssize_t len6 = sizeof(name6);
    struct ipv6_mreq mreq6;

    sock6 = socket(AF_INET6, SOCK_DGRAM, 0);
    if (sock6 == -1) {
        perror("opening v6 listening socket");
        goto die;
    }

    err = setsockopt(sock6, IPPROTO_IPV6, IPV6_V6ONLY, &yes, sizeof(int));
    if (err != 0) {
        perror("setting IPV6_ONLY on sock6");
        goto die;
    }

    memset(&name6, 0, len6);

    name6.sin6_family = PF_INET6;
    name6.sin6_addr = in6addr_any;
    name6.sin6_port = htons(port);

    if (bind(sock6, (struct sockaddr *) &name6, len6) == -1) {
        perror("binding to v6 socket");
        goto die;
    }

    memset(&mreq6, 0, sizeof(struct ipv6_mreq));
    err = inet_pton(AF_INET6, multicast_addr, &mreq6.ipv6mr_multiaddr);
    if (err < 1) {
        fprintf(stderr, "failed to convert %s to an address\n", multicast_addr);
        goto die;
    }
    mreq6.ipv6mr_interface = if_nametoindex(iface_name);

    err = setsockopt(sock6, IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq6,
                     sizeof(struct ipv6_mreq));
    if (err != 0) {
        perror("joining v6 multicast group");
        goto die;
    }


    return sock6;

die:
    if (sock6 != -1) {
        close(sock6);
    }
    return -1;
}

static int setup_v4_socket(const char *multicast_addr,
                           const char *iface_name,
                           int port)
{
    int err;
    int sock4 = -1;
    struct sockaddr_in name4;
    ssize_t len4 = sizeof(name4);
    struct ip_mreqn mreq4;

    sock4 = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock4 == -1) {
        perror("creating v4 socket");
        goto die;
    }

    memset(&name4, 0, len4);

    name4.sin_family = PF_INET;
    name4.sin_addr.s_addr = INADDR_ANY;
    name4.sin_port = htons(port);

    if (bind(sock4, (struct sockaddr *) &name4, len4) == -1) {
        perror("binding to v4 socket");
        goto die;
    }

    memset(&mreq4, 0, sizeof(struct ip_mreqn));
    mreq4.imr_multiaddr.s_addr = inet_addr(V4_MULTICAST_ADDR);
    mreq4.imr_ifindex = if_nametoindex(iface_name);

    err = setsockopt(sock4, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq4,
                     sizeof(struct ip_mreqn));
    if (err != 0) {
        perror("joining v4 multicast group");
        goto die;
    }

    return sock4;

die:
    if (sock4 != -1) {
        close(sock4);
    }
    return -1;
}

int main(int argc, char **argv)
{
    int err;
    int sock6 = -1;
    int sock4 = -1;
    struct sockaddr_storage peer_addr;
    uint8_t buf[1024];
    ssize_t msg_len;
    socklen_t peer_addr_len;
    char host[NI_MAXHOST], service[NI_MAXSERV];
    fd_set read_fds;

    sock6 = setup_v6_socket(V6_MULTICAST_ADDR, IFACE_NAME, LLMNR_PORT);
    if (sock6 == -1) {
        fprintf(stderr, "Failed to set up v6 socket");
        goto die;
    }

    sock4 = setup_v4_socket(V4_MULTICAST_ADDR, IFACE_NAME, LLMNR_PORT);
    if (sock4 == -1) {
        fprintf(stderr, "Failed to set up v4 socket");
        goto die;
    }

    while(true) {
        int nfds = 0;
        int tmp_sock;

        FD_ZERO(&read_fds);

        FD_SET(sock6, &read_fds);
        nfds = max(sock6, nfds);

        FD_SET(sock4, &read_fds);
        nfds = max(sock4, nfds);

        err = select(nfds + 1, &read_fds, NULL, NULL, NULL);

        if (err == -1 && errno == EINTR) {
            continue;
        }

        if (err == -1) {
            perror("select");
            goto die;
        }

        if (FD_ISSET(sock6, &read_fds)) {
            tmp_sock = sock6;
        } else {
            tmp_sock = sock4;
        }

        peer_addr_len = sizeof(struct sockaddr_storage);
        msg_len = recvfrom(tmp_sock, buf, 1024, 0,
                           (struct sockaddr *)&peer_addr, &peer_addr_len);
        if (msg_len == -1) {
            perror("receiving data");
            continue;
        }

        buf[2] |= 0x80; /* We're a reply, honest! */

        err = getnameinfo((struct sockaddr *)&peer_addr, peer_addr_len,
                          host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV);

        if (err == 0) {
            printf("Got %ld bytes from %s:%s\n", (long) msg_len,
                   host, service);
        } else {
            fprintf(stderr, "getnameinfo: %s\n", gai_strerror(err));
            continue;
        }

        if(sendto(tmp_sock, buf, msg_len, 0,
                  (struct sockaddr *)&peer_addr, peer_addr_len) != msg_len) {
            perror("sending reply");
            goto die;
        } else {
            printf("Sent %ld bytes back\n", (long) msg_len);
        }

    }

    close(sock4);
    close(sock6);
    return 0;

die:

    if (sock6 != -1) {
        close(sock6);
    }
    if (sock4 != -1) {
        close(sock4);
    }
    exit(1);
}

