/* Multicast test, sender */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define V6_MULTICAST_ADDR "ff02::1:3"
#define V4_MULTICAST_ADDR "224.0.0.252"
#define IFACE_NAME "br0"
#define LLMNR_PORT "5355"

int do_request(const char *multicast_addr,
               const char *iface_name,
               const char *port,
               const uint8_t *data,
               size_t data_len)
{
    int err;
    int sock = -1;
    int idx = if_nametoindex(iface_name);
    struct addrinfo hints, *ai, *pai;
    ssize_t nread;
    uint8_t reply[1024];

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    err = getaddrinfo(multicast_addr, port, &hints, &ai);
    if (err != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err));
        return -1;
    }

    for (pai = ai; pai != NULL; pai = pai->ai_next) {
        sock = socket(pai->ai_family, pai->ai_socktype, pai->ai_protocol);
        if (sock == -1) {
            continue;
        }

        if (pai->ai_family == AF_INET6) {
            err = setsockopt(sock, IPPROTO_IPV6, IPV6_MULTICAST_IF, &idx, sizeof(idx));
        } else {
            struct ip_mreqn mreq;
            memset(&mreq, 0, sizeof(struct ip_mreqn));

            mreq.imr_multiaddr.s_addr = inet_addr(multicast_addr);
            mreq.imr_ifindex = idx;

            err = setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, &mreq,
                             sizeof(struct ip_mreqn));
        }
        if (err != 0) {
            close(sock);
            continue;
        }

        if (sendto(sock, data, data_len, 0,
                   pai->ai_addr, pai->ai_addrlen) == data_len) {
            break; /* success, break out of loop */
        }

        perror("writing data");
        close(sock);
    }

    freeaddrinfo(ai);
    fprintf(stderr, "sent data...");

    nread = recvfrom(sock, reply, 1024, 0, NULL, NULL);
    if (nread == -1) {
        perror("reading reply");
        return -1;;
    }
    fprintf(stderr, "got reply\n");

    close(sock);
    return 0;
}


int main(int argc, char **argv)
{
    int err;
    uint8_t data[] = {
        0x17, 0x2a, /* id */
        0x00, 0x00, /* flags */
        0x00, 0x01, /* 1 question (big endian) */
        0x00, 0x00, /* 0 answers */
        0x00, 0x00, /* 0 authority records */
        0x00, 0x00, /* 0 additional records */
        0x09, /* size of string 'win2k8-dc' */
        'w', 'i', 'n', '2', 'k', '8', '-', 'd', 'c', 0x00, /* string with terminator */
        0x00, 0x1c, /* AAAA record */
        0x00, 0x01 /* IN class */
    };

    err = do_request(V6_MULTICAST_ADDR, IFACE_NAME, LLMNR_PORT, data,
                     sizeof(data));
    if (err != 0) {
        fprintf(stderr, "Failed to sent v6 multicast query\n");
        exit(1);
    }

    data[24] = 0x01;

    err = do_request(V4_MULTICAST_ADDR, IFACE_NAME, LLMNR_PORT, data,
                     sizeof(data));
    if (err != 0) {
        fprintf(stderr, "Failed to sent v4 multicast query\n");
        exit(1);
    }

    return 0;
}

